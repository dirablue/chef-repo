#
# Cookbook Name:: ruby-on-rails
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

%w{libyaml-devel sqlite-devel}.each do |pkg|
  package pkg do
    action :install
  end
end

#package "libyaml-devel" do
#  action :install
#end

#bash "gem-update" do
#  code "gem update"
#end

bash "add resolv.conf" do
  not_if 'grep "single-request-reopen" /etc/resolv.conf'
  code <<-EOL
    echo 'options single-request-reopen' >> /etc/resolv.conf
  EOL
end

gem_package "rails" do
  action :install
  options("--no-ri --no-rdoc")
end

#bash "install-rails" do
#  no_if 'which rails'
#  code <<-EOC
#    gem update --system
#    gem install --no-ri --no-rdoc rails
#  EOC
#end
