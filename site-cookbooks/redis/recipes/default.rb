#
# Cookbook Name:: redis
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package "redis" do
  action :install
end

service "redis" do
  supports :start => true, :restart => true, :reload => true, :stop => true
  action [ :enable, :start ]
end

