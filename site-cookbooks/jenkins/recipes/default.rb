#
# Cookbook Name:: jenkins
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

execute "jenkins-install" do
  command <<-END
    echo "installing jenkins"
    sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
    sudo rpm --import http://pkg.jenkins-ci.org/redhat/jenkins-ci.org.key
  END
  action :run
end

package "jenkins" do
  action :install
end

service "jenkins" do
  action [ :enable, :start ]
end

