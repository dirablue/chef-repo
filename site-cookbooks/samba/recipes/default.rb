#
# Cookbook Name:: samba
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package "samba" do
  action :install
end

bash "add-samba-user" do
  not_if "pdbedit -L | grep vagrant"
  code <<-EOL
    pdbedit -a vagrant
  EOL
end

bash "samba-dir" do
  not_if "ls /home | grep public"
  code <<-EOL
    sudo mkdir /home/public
    sudo chmod 777 /home/public
  EOL
end

template "smb.conf" do
  path "/etc/samba/smb.conf"
  source "smb.conf"
  owner "root"
  group "root"
end

service "smb" do
  action [ :enable, :start ]
end

