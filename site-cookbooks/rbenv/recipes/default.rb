#
# Cookbook Name:: rbenv
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

%w{gcc make openssl openssl-devel}.each do |pkg|
  package pkg do
    action :install
  end
end 

git "/usr/local/rbenv" do
  not_if 'which rbenv'
  repository "git://github.com/sstephenson/rbenv.git"
  reference "master"
  action :checkout
end

bash "install-rbenv" do
  not_if 'which rbenv'
  code <<-EOC
    echo 'export RBENV_ROOT="/usr/local/rbenv"' >> /etc/bashrc
    echo 'export PATH="$PATH:/usr/local/rbenv/bin"' >> /etc/bashrc
    echo 'eval "$(rbenv init -)"' >> /etc/bashrc
    source /etc/bashrc
  EOC
end

git "/tmp/ruby-build" do
  repository "git://github.com/sstephenson/ruby-build.git"
  reference "master"
  action :checkout
end

bash "install-rubybuild" do
  not_if 'which ruby-build'
  code <<-EOC
    cd /tmp/ruby-build
    ./install.sh
  EOC
end

bash "install-ruby" do
  code <<-EOL
    source /etc/bashrc
    gem update bundler
    rbenv install 2.1.2
    rbenv global 2.1.2
    chmod o+w shims
    rbenv rehash
  EOL
end

