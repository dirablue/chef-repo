#
# Cookbook Name:: sudo-path
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

bash "sudo-path" do
  not_if 'sudo -l | grep "PATH"'
  code <<-EOL
    echo 'Defaults env_keep += "PATH"' >> /etc/sudoers
  EOL
end

ruby_block "off secure_path" do
  only_if 'sudo -l | grep secure_path'
  block do
    f = Chef::Util::FileEdit.new("/etc/sudoers")
    f.search_file_replace(/^Defaults *secure_path/,"#Defaults  secure_path")
    f.write_file
  end
end
